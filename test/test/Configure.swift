//
//  Configure.swift
//  test
//
//  Created by Paul Crowell on 1/8/19.
//  Copyright © 2019 Paul Crowell. All rights reserved.
//

import Foundation

// cheap logging
func DLogInfo(_ msg: String) { NSLog("Info: \(#function): \(msg)") }
func DLogWarning(_ msg: String) { NSLog("Warning: \(#function): \(msg)") }
func DLogError(_ msg: String) { NSLog("Error: \(#function): \(msg)") }

