//
//  Smallest.swift
//  test
//
//  Created by Paul Crowell on 1/8/19.
//  Copyright © 2019 Paul Crowell. All rights reserved.
//

import Foundation

//Write a function:

//public func solution(_ A : inout [Int]) -> Int

//that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

//For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

//Given A = [1, 2, 3], the function should return 4.

//Given A = [−1, −3], the function should return 1.

//Write an efficient algorithm for the following assumptions:

//N is an integer within the range [1..100,000];
//each element of array A is an integer within the range [−1,000,000..1,000,000].

public func checkIfUnitMissingIn(_ aSet: inout Set<Int>) -> Bool {
    if !aSet.contains(1) {
        DLogInfo("\(aSet): missing 1.")
        return true
    }
    return false
}

@discardableResult public func checkIfNegativeIn(_ aSorted: inout [Int]) -> Bool {
    var negative = false
    for i in 0..<aSorted.count {
        if aSorted[i] < 1 {
            negative = true
            DLogInfo("\(aSorted): has values less that 1.")
            break
        }
    }
    return negative
}

public func checkMissingIntExistsIn(_ aSorted: inout [Int]) -> Int {
    var result = 0
    /// Case 1: Simple unit missing from set: return 1
    var aSet: Set = Set(aSorted)
    if aSorted[0] == 1 && aSorted.count == 1 {
        result = aSorted[0] + 1
        return result
    }
    if checkIfUnitMissingIn(&aSet) {
        result = 1
        DLogInfo("\(aSorted): inserting missing 1: \(result).")
        return result
    }
    // NOTE: If we got here, array has 1 or more values and has a 1.
    // Covers all corner cases of array size, breaks in continuous data.
    if aSorted.count > 1 {
        /// Case 4: single value arrays don't have missing ints other than 1
        /// Case 2: Intermediate integer missing from array with 2 or more
        /// values: return smallest int missing
        for i in 0..<aSorted.count {
            var present, next: Int
            present = aSorted[i] // this value
            if aSorted[i] <= 1 { continue } // negative, 0 or 1
            if aSorted.count > 1 && i >= 0
                && aSorted[i] != aSorted.last { // for arrays of 2 or more values
                next = aSorted[i+1]
            } else { // for arrays of a single value
                next = present
            }
            if next == present + 1 { continue } // any consecutive values
            if next > present + 1 { // break in sequence, return next missing integer
                result = aSorted[i] + 1
                break
            }
            if next == present { // last in sequence, return next missing integer
                result = aSorted[i] + 1
                break
            }
        }
    }
    return result
}

public func smallestIntFrom(_ A : inout [Int]) -> Int {
    let aSet: Set = Set(A)
    var aSorted = aSet.sorted {$0 < $1}
    var result = 0
    
    /// Cases
    /// Case 1: Any value negative: return snallest positive int or 1 if 1 is missing
    checkIfNegativeIn(&aSorted)
    /// Case 2: Intermediate integer missing: return 1 or smallest int missing
    result = checkMissingIntExistsIn(&aSorted)
    return result
}

public func solution(_ A : inout [Int]) -> Int {
    return smallestIntFrom(&A)
}
