//
//  testSmallestIntTests.swift
//  testSmallestIntTests
//
//  Created by Paul Crowell on 1/8/19.
//  Copyright © 2019 Paul Crowell. All rights reserved.
//

import Foundation
import XCTest
@testable import test

class testSmallestIntTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testAllValuesSpecifiedArrays() {
        // All values specified
        var A = [1, 2, 3]
        var B = [2, 3]
        XCTAssertEqual(4, smallestIntFrom(&A))
        XCTAssertEqual(1, smallestIntFrom(&B))
    }
    
    func testMissingValuesArrays() {
        // missing values
        var C = [1, 3, 6, 4, 1, 2]
        var D = [-1, -3]
        var E = [1]
        var F = [2]
        var G = [0]
        var H = [-1]
        var I = [2, 4, 5, 6]
        var J = [1, 2, 4, 5, 6]
        XCTAssertEqual(5, smallestIntFrom(&C))
        XCTAssertEqual(1, smallestIntFrom(&D))
        XCTAssertEqual(2, smallestIntFrom(&E))
        XCTAssertEqual(1, smallestIntFrom(&F))
        XCTAssertEqual(1, smallestIntFrom(&G))
        XCTAssertEqual(1, smallestIntFrom(&H))
        XCTAssertEqual(1, smallestIntFrom(&I))
        XCTAssertEqual(3, smallestIntFrom(&J))
    }
    
    func testExtremeMinMaxValuesArrays() {
        //extreme_min_max_value
        var K = [-1000000, 1000000]
        var L = [-1000000, 1, 1, 1, 2, 2, 3, 4, 5, 1000000]
        var M = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200]
        var N = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200]
        XCTAssertEqual(1, smallestIntFrom(&K))
        XCTAssertEqual(6, smallestIntFrom(&L))
        XCTAssertEqual(101, smallestIntFrom(&M))
        XCTAssertEqual(201, smallestIntFrom(&N))
    }
    
    func testNegativeOnlyValuesArrays() {
        //negative_only
        var O = [-100, -99, -98, -97, -96, -95, -94, -93, -92, -91, -90, -89, -88, -87, -86, -85, -84, -83, -82, -81, -80, -79, -78, -77, -76, -75, -74, -73, -72, -71, -70, -69, -68, -67, -66, -65, -64, -63, -62, -61, -60, -59, -58, -57, -56, -55, -54, -53, -52, -51, -50, -49, -48, -47, -46, -45, -44, -43, -42, -41, -40, -39, -38, -37, -36, -35, -34, -33, -32, -31, -30, -29, -28, -27, -26, -25, -24, -23, -22, -21, -20, -19, -18, -17, -16, -15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1]
        XCTAssertEqual(1, smallestIntFrom(&O))
    }

}
