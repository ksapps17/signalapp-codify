import Foundation

// cheap logging
func DLogInfo(_ msg: String) { NSLog("Info: \(#function): \(msg)") }
func DLogWarning(_ msg: String) { NSLog("Warning: \(#function): \(msg)") }
func DLogError(_ msg: String) { NSLog("Error: \(#function): \(msg)") }

// Task #3 of 3: Write a function solution that, given a long piece of text: S, you want to send it to a friend as a series of SMS messages. Upper and lower chars, no leading, trailing or double spaces to remove. Split into chunks of length less than or equal to K chars, the overall message length in chars and return number of chunks of K to transmit the message. Cannot split at a word boundary.

public func solution(_ S : inout String, _ K : Int) -> Int {
    var result : Int = 0
    let arrayFromS = S.split(separator: " ")
    // create a messages array to hold all messages
    var messages : Array<String> = []
    var message : String = ""
    let aSpace : String = " "
    // Test for failure and exit if any word is longer than K, no legal messages could be created.
    let ixFail : Int? = arrayFromS.index(where: {$0.count > K})
    if ixFail != nil {
        DLogError("Impossible to form SMS with limit K: \(K).")
        return 0
    }
    for i in 0..<arrayFromS.count {
        // create a new message array for each message, <= K long
        // pop a word off word array, if size of word + message <= K, append word to message
        // else append message to messages array and create a new message array.
        let nextWord = arrayFromS[i]
        if message.count + nextWord.count <= K {
            if message.count + nextWord.count == K ||
                message.count + nextWord.count == K-1 {
                message += nextWord
            } else {
                message += nextWord + aSpace
            }
        } else {
            messages.append(message)
            if message.count + nextWord.count <= K {
                message = message + aSpace
            } else {
                if i == arrayFromS.count - 1 {
                    message = String(nextWord)
                } else {
                    message = nextWord + aSpace
                }
            }
        }
    }
    messages.append(message)
    result = messages.count
    return result
}

var aBuffer1 : String = "SMS messages are really short"
var aBuffer2 : String = "Write a function solution that, given a long piece of text: S, you want to send it to a friend as a series of SMS messages."
var result1 : Int = 0
var result2 : Int = 0

result1 = solution(&aBuffer1, 12)
if result1 != 3 {
    DLogError("solution for aBuffer1 with K of 12 should be 3: \(result1): FAIL.")
} else {
    DLogInfo("solution for aBuffer1 with K of 12 should be 3: CORRECT.")
}

result2 = solution(&aBuffer2, 12)
if result2 != 12 {
    DLogError("solution for aBuffer2 with K of 12 should be 12: \(result2): FAIL..")
} else {
    DLogInfo("solution for aBuffer2 with K of 12 should be 12: CORRECT.")
}
