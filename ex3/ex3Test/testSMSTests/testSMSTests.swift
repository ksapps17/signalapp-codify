//
//  testSMSTests.swift
//  testSMSTests
//
//  Created by Paul Crowell on 1/11/19.
//  Copyright © 2019 Paul Crowell. All rights reserved.
//

import Foundation
import XCTest
@testable import test

class testSMSTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSMSBuffers() {
        var aBuffer1 : String = "SMS messages are really short"
        var aBuffer2 : String = "Write a function solution that, given a long piece of text: S, you want to send it to a friend as a series of SMS messages."
        XCTAssertEqual(3, solution(&aBuffer1, 12))
        XCTAssertEqual(12, solution(&aBuffer2, 12))
    }
}
