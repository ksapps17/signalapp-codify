//
//  AppDelegate.swift
//  test
//
//  Created by Paul Crowell on 1/4/19.
//  Copyright © 2019 Paul Crowell. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        var aBuffer1 : String = "SMS messages are really short"
        var aBuffer2 : String = "Write a function solution that, given a long piece of text: S, you want to send it to a friend as a series of SMS messages."
        
        var result1 : Int = 0
        var result2 : Int = 0
        
        result1 = solution(&aBuffer1, 12)
        if result1 != 3 {
            DLogError("solution for aBuffer1 with K of 12 should be 3: \(result1): FAIL.")
        } else {
            DLogInfo("solution for aBuffer1 with K of 12 should be 3: CORRECT.")
        }
        
        result2 = solution(&aBuffer2, 12)
        if result2 != 12 {
            DLogError("solution for aBuffer2 with K of 12 should be 12: \(result2): FAIL..")
        } else {
            DLogInfo("solution for aBuffer2 with K of 12 should be 12: CORRECT.")
        }

        print("solution for aBuffer1 with K of 12: \(solution(&aBuffer1, 12)) should be 3.")
        print("solution for aBuffer2 with K of 12: \(solution(&aBuffer2, 12)) should be 12.")
        
        return true
    }

}
