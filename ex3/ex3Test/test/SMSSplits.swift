//
//  SMSSplits.swift
//  test
//
//  Created by Paul Crowell on 1/8/19.
//  Copyright © 2019 Paul Crowell. All rights reserved.
//

import Foundation

// Write a function solution that, given a long piece of text: S, you want to send it to a friend as a series of SMS messages. Upper and lower chars, no leading, trailing or double spaces to remove. Split into chunks of length less than or equal to K chars, the overall message length in chars and return number of chunks of K to transmit the message. Cannot split at a word boundary.

public func solution(_ S : inout String, _ K : Int) -> Int {
    var result : Int = 0
    let arrayFromS = S.split(separator: " ")
    // create a messages array to hold all messages
    var messages : Array<String> = []
    var message : String = ""
    let aSpace : String = " "
    // test for failure and exit if any word is longer that K
    let ixFail : Int? = arrayFromS.index(where: {$0.count > K})
    if ixFail != nil {
        DLogError("Impossible to form SMS with limit K: \(K).")
        return 0
    }
    for i in 0..<arrayFromS.count {
        // create a new message array for each message, <= K long
        // pop a word off word array, if size of word + message <= K, append word to message
        // else append message to messages array and create a new message array.
        let nextWord = arrayFromS[i]
        print("nextWord: \"\(nextWord)\".")
        if message.count + nextWord.count <= K {
            if message.count + nextWord.count == K ||
                message.count + nextWord.count == K-1 {
                message += nextWord
            } else {
                message += nextWord + aSpace
            }
            print("message: \"\(message)\" of length: \(message.count).")
        } else {
            messages.append(message)
            if message.count + nextWord.count <= K {
                message = message + aSpace
            } else {
                if i == arrayFromS.count - 1 {
                    message = String(nextWord)
                } else {
                    message = nextWord + aSpace
                }
            }
            print("new message: \"\(message)\" of length: \(message.count).")
            print("messages: \(messages) of length: \(messages.count), so far.")
        }
    }
    messages.append(message)
    print("messages: \(messages) of length: \(messages.count), so far.")
    print("messages made: \(messages.count).")
    result = messages.count
    return result
}
