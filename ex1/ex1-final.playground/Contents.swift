import Foundation

func DLogInfo(_ msg: String) { NSLog("Info: \(#function): \(msg)") }

// Task #1 of 3: Write a function solution that, given strings name and surname and integer age, returns a string composed of the first two letters from each of name and surname followed by the age.

public func solution(_ name : inout String, _ surname : inout String, _ age : Int) -> String {
    let newString : String = name.prefix(2) + surname.prefix(2) + "\(age)"
    return newString
}

var aName : String = "Paul"
var aSurname : String = "Smith"
var anAge : Int = 19
// expect: PaSm19
print("solution: \(solution(&aName, &aSurname, anAge))")
