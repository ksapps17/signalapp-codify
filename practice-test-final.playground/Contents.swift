import Foundation

func DLogInfo(_ msg: String) { NSLog("Info: \(#function): \(msg)") }

public func testArray(_ A: inout [Int], expect: Int) {
    // dups we need locally, but not to be delivered
    let aSet: Set = Set(A)
    var aSorted = aSet.sorted {$0 < $1}
    print("aSorted: \(aSorted), solution: \(solution(&aSorted), debug: false)) vs. expect: \(expect)")
}

public func checkIfUnitMissingIn(_ aSet: inout Set<Int>) -> Bool {
    if !aSet.contains(1) {
        DLogInfo("\(aSet): missing 1.")
        return true
    }
    return false
}

public func checkIfNegativeIn(_ aSorted: inout [Int]) -> Bool {
    var negative = false
    for i in 0..<aSorted.count {
        if aSorted[i] < 1 {
            negative = true
            DLogInfo("\(aSorted): has values less that 1.")
            break
        }
    }
    return negative
}

public func checkMissingIntExistsIn(_ aSorted: inout [Int]) -> Int {
    var result = 0
    var otherMissing = false
    /// Case 1: Simple unit missing from set: return 1
    var aSet: Set = Set(aSorted)
    if aSorted[0] == 1 && aSorted.count == 1 {
        result = aSorted[0] + 1
        return result
    }
    if checkIfUnitMissingIn(&aSet) {
        result = 1
        DLogInfo("\(aSorted): inserting missing 1: \(result).")
        return result
    }
    // NOTE: If we got here, array has 1 or more values and has a 1.
    // Covers all corner cases of array size, breaks in continuous data.
    if aSorted.count > 1 {
        /// Case 4: single value arrays don't have missing ints other than 1
        /// Case 2: Intermediate integer missing from array with 2 or more
        /// values: return smallest int missing
        for i in 0..<aSorted.count {
            var present, next: Int
            present = aSorted[i]             // this value
            if aSorted[i] <= 1 { continue }  // negative, 0 or 1
            if aSorted.count > 1 && i >= 0
                && aSorted[i] != aSorted.last { // for arrays of 2 or more values
                next = aSorted[i+1]
            } else {                         // for arrays of a single value
                next = present
            }
            if next == present + 1 { continue } // any consecutive values
            if next > present + 1 { // break in sequence, return next missing integer
                otherMissing = true
                result = aSorted[i] + 1
                break
            }
            if next == present { // last in sequence, return next missing integer
                result = aSorted[i] + 1
                break
            }
        }
    }
    if otherMissing {
        DLogInfo("\(aSorted): missing value of: \(result).")
    } else {
        DLogInfo("\(aSorted): no missing values. Appending: \(result).")
    }
    return result
}

public func solution(_ A : inout [Int], debug : Bool = false) -> Int {
    var aSorted = A.sorted {$0 < $1}
    var valueIsNegative: Bool = false
    var result = 0
    
    /// Cases
    /// Case 1: Any value negative: return snallest positive int or 1 if 1 is missing
    valueIsNegative = checkIfNegativeIn(&aSorted)
    /// Case 2: Intermediate integer missing: return 1 or smallest int missing
    result = checkMissingIntExistsIn(&aSorted)
    return result
}

// All values specified
var A = [1, 2, 3]
var B = [2, 3]
testArray(&A, expect: 4)
testArray(&B, expect: 1)

// missing values
var C = [1, 3, 6, 4, 1, 2]
var D = [-1, -3]
var E = [1]
var F = [2]
var G = [0]
var H = [-1]
var I = [2, 4, 5, 6]
var J = [1, 2, 4, 5, 6]
testArray(&C, expect: 5)
testArray(&D, expect: 1)
testArray(&E, expect: 2)
testArray(&F, expect: 1)
testArray(&G, expect: 1)
testArray(&H, expect: 1)
testArray(&I, expect: 1)
testArray(&J, expect: 3)

//extreme_min_max_value
var K = [-1000000, 1000000]
var L = [-1000000, 1, 1, 1, 2, 2, 3, 4, 5, 1000000]
var M = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200]
var N = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200]
testArray(&K, expect: 1)
testArray(&L, expect: 6)
testArray(&M, expect: 101)
testArray(&N, expect: 201)

//negative_only
var O = [-100, -99, -98, -97, -96, -95, -94, -93, -92, -91, -90, -89, -88, -87, -86, -85, -84, -83, -82, -81, -80, -79, -78, -77, -76, -75, -74, -73, -72, -71, -70, -69, -68, -67, -66, -65, -64, -63, -62, -61, -60, -59, -58, -57, -56, -55, -54, -53, -52, -51, -50, -49, -48, -47, -46, -45, -44, -43, -42, -41, -40, -39, -38, -37, -36, -35, -34, -33, -32, -31, -30, -29, -28, -27, -26, -25, -24, -23, -22, -21, -20, -19, -18, -17, -16, -15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1]
testArray(&O, expect: 1)
